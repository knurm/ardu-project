/*  This file is part of ardu-projct.
    
    Copyright (C) 2016  Kertu Nurmberg

    Ardu-project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ardu-project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <avr/interrupt.h>
#include "uart-wrapper.h"
#include "hmi_msg.h"
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "cli_microrl.h"
#include "rfid.h"


#define BAUD 9600
#define BLINK_DELAY_MS 100
#define UART_STATUS_MASK 0x00FF


#define LED_INIT DDRA |= _BV(DDA3)
#define DOOR_INIT DDRA |= _BV(DDA1)
#define LED_TOGGLE PORTA ^= _BV(PORTA3)
#define DOOR_OPEN PORTA |= _BV(PORTA1)
#define DOOR_CLOSE PORTA &= ~_BV(PORTA1)


volatile uint32_t counter;

// Create microrl object and pointer on it
static microrl_t rl;
static microrl_t *prl = &rl;


static inline void init_system_clock(void)
{
    TCCR5A = 0; // Clear control register A
    TCCR5B = 0; // Clear control register B
    TCCR5B |= _BV(WGM52) | _BV(CS52); // CTC and fCPU/256
    OCR5A = 62549; // 1 s
    TIMSK5 |= _BV(OCIE5A); // Output Compare A Match Interrupt Enable
}


static inline void init_rfid_reader(void)
{
    /* Init RFID-RC522 */
    MFRC522_init();
    PCD_Init();
}


static inline void init_hw(void)
{
    // set pin 3 of PORTA for output
    LED_INIT;
    DOOR_INIT;
    // Init error console as stderr in UART3
    uart0_init(UART_BAUD_SELECT(BAUD, F_CPU));
    uart3_init(UART_BAUD_SELECT(BAUD, F_CPU));
    init_system_clock();
    sei();
    stdin = stdout = &uart0_io;
    stderr = &uart3_out;
    // End UART3 init
    lcd_init();
    lcd_home();
    init_rfid_reader();
}


static inline void name_print()
{
    fprintf_P(stdout, PSTR("\r" STUD_NAME "\n"));
    lcd_puts_P(PSTR(STUD_NAME));
}


static inline void start_cli(void)
{
    // Call init with ptr to microrl instance and print callback
    microrl_init (prl, cli_print);
    //Set callback for execute
    microrl_set_execute_callback (prl, cli_execute);
}


static inline void heartbeat()
{
    static uint32_t last_time;
    uint32_t current_time;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        current_time = counter;
    }

    if ((last_time - current_time) > 0) {
        /* Toggle LED in Arduino Mega pin 25 */
        PORTA ^= _BV(PORTA3);
        fprintf_P(stderr, PSTR(UPTIME "\n"), current_time);
    }

    last_time = current_time;
}


static inline void handle_door()
{
    Uid uid;
    card_t card;
    uint32_t time_cur;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        time_cur = counter;
    }
    static uint32_t message_start;
    static uint32_t door_open_start;

    if (PICC_IsNewCardPresent()) {
        PICC_ReadCardSerial(&uid);
        card.uid_size = uid.size;
        memcpy(&card.uid, &uid.uidByte, uid.size);
        card.user = NULL;
        card_t *found_card = rfid_find_card(&card);

        if (found_card) {
            lcd_goto(0x40);
            lcd_puts(found_card->user);

            for (int8_t i = 16 - strlen(found_card->user); i > -1; i--) {
                lcd_putc(' ');
            }

            DOOR_OPEN;
        } else {
            DOOR_CLOSE;
            lcd_goto(0x40);
            lcd_puts(ACCESS_DENIED_MSG);

            for (int8_t i = 4; i > -1; i--) {
                lcd_putc(' ');
            }
        }

        door_open_start = time_cur;
        message_start = time_cur;
    }

    if ((message_start + 5) < time_cur) {
        lcd_goto(0x40);

        for (int8_t i = 16; i > -1; i--) {
            lcd_putc(' ');
        }
    }

    if ((door_open_start + 2) < time_cur) {
        DOOR_CLOSE;
    }
}


int main (void)
{
    init_hw();
    version_print(stderr);
    name_print();
    start_cli();

    while (1) {
        heartbeat();
        // CLI commands are handled in cli_execute()
        microrl_insert_char (prl, uart0_getc() & UART_STATUS_MASK);
    }
}


/* System clock ISR */
ISR(TIMER5_COMPA_vect)
{
    counter++;
}
