/*  This file is part of ardu-projct.
    
    Copyright (C) 2016  Kertu Nurmberg

    Ardu-project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ardu-project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_


#define STUD_NAME "Kertu Nurmberg"
#define VERSION_STRING "Version: %S built on: %S %S\n"
#define AVR_VERSION_STRING "avr-libc version: %S avr-gcc version:  %S\n"
#define FULL_DISPLAY_WIDTH_WHITESPACE_CHARACTER "                "
#define UPTIME "Uptime: %lu s"

#define HELP_CMD "help"
#define HELP_HELP "Get help"

#define VER_CMD "version"
#define VER_HELP "Print FW version"

#define ASCII_CMD "ascii"
#define ASCII_HELP "print ASCII tables"

#define MONTH_CMD "month"
#define MONTH_HELP "Find matching month from lookup list. Usage: month <string>"

#define CLI_HELP_MSG "Implemented commands:"
#define CLI_NO_CMD "Command not implemented.\n Use <help> to get help.\n"
#define CLI_ARG_MSG "To few or to many arguments for this command\nUse <help>\n"

#define READ_CMD "read"
#define READ_HELP "Read and print out card info that is currently\nin proximity of the reader"

#define ADD_CMD "add"
#define ADD_HELP "<add> <name> adds a new card to the system"

#define REMOVE_CMD "remove"
#define REMOVE_HELP "<remove> <name> removes the card from the system"

#define LIST_CMD "list"
#define LIST_HELP "<list> lists all the cards in the system"

#define ACCESS_DENIED_MSG "Access denied!"
#define NOT_ADDING_CARD_MSG1 "Card \""
#define NOT_ADDING_CARD_MSG2 "\" already exists.\n"
#define OUT_OF_MEMORY_MSG "Out of memory. Please remove cards."
#define NO_CARDS_ADDED_MSG "No cards added"
#define CARD_NOT_FOUND_MSG "Card not found"
#define LINKED_LIST_ERROR_MSG "Invalid situation when removing card"
#define UNABLE_TO_DETECT_CARD_MSG "Unable to detect card."
#define CARD_SELECTED_MSG "Card selected!"
#define UID_SIZE_MSG "UID size: 0x%02X"
#define UID_SAK_MSG "UID sak: 0x%02X"
#define CARD_UID_MSG "Card UID: "
#define CARD_NOT_SELECTED "Unable to select card.\n"


extern PGM_P const nameMonth[];

extern const char help_cmd[];
extern const char help_help[];
extern const char ver_cmd[];
extern const char ver_help[];
extern const char ascii_cmd[];
extern const char ascii_help[];
extern const char month_cmd[];
extern const char month_help[];
extern const char read_cmd[];
extern const char read_help[];
extern const char add_cmd[];
extern const char add_help[];
extern const char remove_cmd[];
extern const char remove_help[];
extern const char list_cmd[];
extern const char list_help[];


#endif /* _HMI_MSG_H */
